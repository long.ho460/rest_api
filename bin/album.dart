class Album {
  final userId;
  final id;
  final title;

  Album({
    this.userId,
    this.id,
    this.title,
  });

  factory Album.fromJson(Map<String, dynamic> json) {
    return Album(
      userId: json['userId'],
      id: json['id'],
      title: json['title'],
    );
  }

  @override
    String toString() {
      // TODO: implement toString
      return super.toString();
    }

    
}